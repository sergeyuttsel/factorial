package com.alfabank;

public class Factorial {

    public static void main(String[] args) {
        System.out.println(getFactorial(20));
    }

    static public long getFactorial(long num) {
        if (num == 0) return 1;
        return num * getFactorial(num-1);

    }

}
